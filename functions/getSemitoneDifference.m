function semitoneDiff = getSemitoneDifference(referenceWeight,comparisonWeight)
% map the change in weight to a change in semitones
% enter weights in grams

% semitone change per 100 grams change in weight
semitonesPer100grams = 0.25;

% get the change in distance (in cm)
deltaW = comparisonWeight - referenceWeight;

% convert to a semitone change
semitoneDiff = semitonesPer100grams*deltaW;

end