%% ----- GENERAL HOUSEKEEPING -----
% Clear the workspace
clc; clear; close all;
sca;

% parent folder (changes depending on machine, comment out incorrect paths)
parentFolder = 'C:\Users\User\Dropbox\ERC_Obj_2_MoC\';
% parentFolder = 'D:\Dropbox\ERC_Obj_2_MoC\';

% add the paths to the dependencies and specific functions that we need
addpath(genpath([parentFolder,'functions']))
addpath(genpath([parentFolder,'dependencies']))

%% ----- SET UP PSYCHTOOLBOX -----

% Some default settings
PsychDefaultSetup(2);
Screen('Preference', 'SkipSyncTests', 1);

% Setup Psychtoolbox for OpenGL 3D rendering support and initialize the
% mogl OpenGL for Matlab wrapper
InitializeMatlabOpenGL;

% Random number generator
rng('shuffle');

% Get the screen numbers. This gives us a number for each of the screens
% attached to our computer.
params.screens = Screen('Screens');
  
% Defines the screen to use
params.screenNumber = max(params.screens);
 
% Open a window
[params.window, params.windowRect] = PsychImaging('OpenWindow', params.screenNumber, ...
    0, [], 32, 2);

% Get the size of the on screen window in pixels
[params.screenXpixels, params.screenYpixels] = Screen('WindowSize', params.window);

% Get the centre coordinate of the window in pixels
[params.xCenter, params.yCenter] = RectCenter(params.windowRect);

% Set up alpha-blending for smooth (anti-aliased) edges to our dots
Screen('BlendFunction', params.window, 'GL_SRC_ALPHA', 'GL_ONE_MINUS_SRC_ALPHA');

% set up a quit button
KbName('UnifyKeyNames');
params.keyIDs{1,1} = 'quit';
params.keyIDs{2,1} = 'enter';
params.keyIDs{3,1} = 'lighter';
params.keyIDs{4,1} = 'heavier';
params.keyInd(1,1) = KbName('ESCAPE');
params.keyInd(2,1) = KbName('RETURN');
params.keyInd(3,1) = KbName('l'); 
params.keyInd(4,1) = KbName('h');

% set up keyCode checks
isQuit = @(keyCode) keyCode(params.keyInd(1,1));
isContinue = @(keyCode) keyCode(params.keyInd(2,1));
isLighter = @(keyCode) keyCode(params.keyInd(3,1));
isHeavier = @(keyCode) keyCode(params.keyInd(4,1));
 
% Hide the cursor   
HideCursor; 

% text settings
Screen('TextFont', params.window, 'Ariel');
Screen('TextSize', params.window, 30);

% appearance parameters
params.backColour = [0.3 0.3 0.3];
params.textColour = [0.7 0.7 0.7];

%% ----- GET INFORMATION ABOUT THE PARTICIPANT -----

Screen('FillRect', params.window, [0.3 0.3 0.3]);
part.pID = GetEchoString(params.window, 'Enter Participant ID (ERC_Obj2_Exp1_X):',...
    params.screenXpixels*0.35,params.screenYpixels*0.35,params.textColour,[0.3 0.3 0.3],0,2,[],[]);
part.age = str2double(GetEchoString(params.window, 'Enter Participant Age (in years):',...
    params.screenXpixels*0.35,params.screenYpixels*0.45,params.textColour,[0.3 0.3 0.3],0,2,[],[]));
part.sex = upper(GetEchoString(params.window, 'Enter Participant Sex (at birth, F or M):',...
    params.screenXpixels*0.35,params.screenYpixels*0.55,params.textColour,[0.3 0.3 0.3],0,2,[],[]));
part.group = str2double(GetEchoString(params.window, 'Enter Group ID (1 or 2):',...
    params.screenXpixels*0.35,params.screenYpixels*0.65,params.textColour,[0.3 0.3 0.3],0,2,[],[]));
part.date = datestr(now);

%% ----- SET UP EXPERIMENT -----

[params,data] = setUpTask(params,part);

%% ----- RUN THE EXPERIMENT -----

% Welcome screen and instructions
welcomeText = ['Hello. \n\n',...
    'This is an experiment! \n\n',...
    'These instructions are on multiple lines :D \n\n'];
continueText = 'Press enter to continue.';
Screen('FillRect', params.window, params.backColour);
DrawFormattedText(params.window, welcomeText, 'center', 'center', params.textColour);
DrawFormattedText(params.window, continueText, params.screenXpixels*0.75, ...
    params.screenYpixels*0.9, params.textColour);
Screen('Flip', params.window);

% wait for enter, or to quit
[~,~,keyCode] = KbCheck;
while ~isQuit(keyCode) && ~isContinue(keyCode)
    [~,~,keyCode] = KbCheck;
end

% count down to first trial (may not need this but can be handy sometimes)
secsLeft = 5;
% count down the seconds unless quit key is pressed
while secsLeft > 0 && ~isQuit(keyCode)
    Screen('FillRect', params.window, params.backColour);
    DrawFormattedText(params.window, ['Starting in: ', num2str(secsLeft)],...
        'center', 'center', params.textColour);
    Screen('Flip', params.window);
    pause(1);
    secsLeft = secsLeft - 1;
    [~,~,keyCode] = KbCheck;
end

% start experiment loop
trialCounter = 0;
while trialCounter < size(data.trials,1) && ~isQuit(keyCode)

    % check if we are due a break
    if trialCounter > 0 && ~mod(trialCounter,params.breakInt) && ~isQuit(keyCode)
        % time for a memory dump, just incase...
        save([parentFolder,'memoryDumps\TaskXXXDump_',pID,'_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])
        % take a break
        secsLeft = 5;
        % count down the seconds unless quit key is pressed
        while secsLeft > 0 && ~isQuit(keyCode)
            Screen('FillRect', params.window, params.backColour);
            DrawFormattedText(params.window,...
                ['Take a break! Have a KitKat! \n\n',...
                'Starting again in: ', num2str(secsLeft)],...
                'center', 'center', params.textColour);
            Screen('Flip', params.window);
            pause(1);
            secsLeft = secsLeft - 1;
            [~,~,keyCode] = KbCheck;
        end
        % Let the participant say they are ready to go again
        Screen('FillRect', params.window, params.backColour);
        DrawFormattedText(params.window, 'Press enter to start.',...
            'center', 'center', params.textColour);
        Screen('Flip', params.window);
        % wait for the participant to press enter, or to quit
        [~,~,keyCode] = KbCheck;
        while ~isContinue(keyCode) && ~isQuit(keyCode)
            [~,~,keyCode] = KbCheck;
        end
    end

    % count up the trial counter
    trialCounter = trialCounter + 1;

    % figure out which stimulus (reference or comparison) to show first and
    % which to show second
    if data.trials(trialCounter,4)
        % reference first
        stim_1 = data.trials(trialCounter,2);
        stim_2 = data.trials(trialCounter,3);
    else
        % reference second
        stim_1 = data.trials(trialCounter,3);
        stim_2 = data.trials(trialCounter,2);
    end

    % show instructions for this trial
    Screen('FillRect', params.window, params.backColour);
    DrawFormattedText(params.window, ['Show ',num2str(stim_1),'g first. \n\n',...
        'Show ',num2str(stim_2),'g second. \n\n\n\n\n\n',...
        'Press L if second stimulus reported as LIGHTER. \n\n',...
        'Press H if second stimulus reported as HEAVIER.'],...
        'center', 'center', params.textColour);
    Screen('Flip', params.window);

    % wait for response, or to quit
    [~,~,keyCode] = KbCheck;
    while ~isLighter(keyCode) && ~isHeavier(keyCode) && ~isQuit(keyCode)
        [~,~,keyCode] = KbCheck;
    end

    % process response
    if ~isQuit(keyCode)
        if isLighter(keyCode)
            % if responded lighter and reference was first, comparison is lighter
            if data.trials(trialCounter,4)
                data.trials(trialCounter,5) = 0;
            else % if responded lighter and reference was second, comparison is heavier
                data.trials(trialCounter,5) = 1;
            end
        elseif isHeavier(keyCode)
            % if responded heavier and reference was first, comparison is heavier
            if data.trials(trialCounter,4)
                data.trials(trialCounter,5) = 1;
            else % if responded heavier and reference was second, comparison is lighter
                data.trials(trialCounter,5) = 0;
            end
        end
        % Were they correct?
        data.trials(trialCounter,6) = ...
            (data.trials(trialCounter,3) > data.trials(trialCounter,2) && ... % was comparison heavier?
                data.trials(trialCounter,5)) || ... % and they said so?
                (data.trials(trialCounter,3) < data.trials(trialCounter,2) && ... % or was it lighter?
                ~data.trials(trialCounter,5)); % and they said so?
    end

    % wait for key release if not quitting
    if ~isQuit(keyCode)
        [~,~,keyCode] = KbCheck;
        while isLighter(keyCode) && isHeavier(keyCode) && ~isQuit(keyCode)
            [~,~,keyCode] = KbCheck;
        end
    end

    % small interlude
    Screen('FillRect', params.window, params.backColour);
    Screen('Flip', params.window);
    pause(0.25)

end

%% ----- END AND TIDY UP -----

finishedText = 'Thanks! The task is now complete. We are very grateful for your time.';
Screen('FillRect', params.window, params.backColour);
DrawFormattedText(params.window, finishedText, 'center', 'center', params.textColour);
Screen('Flip', params.window);
pause(3)

% save the data
save([parentFolder,'memoryDumps\TaskXXXDump_',part.pID,'_',datestr(now,'dd-mmm-yyyy-HH-MM-SS')])
save([parentFolder,'data\TaskXXXData_',part.pID],'data','params')

% tidy up
sca;

